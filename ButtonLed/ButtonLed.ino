/*
   Author: Enzo Evers
   Data: 9 August 2017
*/

int button = 8;
int led = 7;

void setup()
{
  pinMode(button, INPUT); // We read the signal from the button.
  pinMode(led, OUTPUT); // And send a signal to the led.
}

void loop()
{
  /*
    'HIGH'  is the same as '1' is the same as 'true'
    'LOW'  is the same as '0' is the same as 'false'
  */

  if (digitalRead(button) == HIGH) // if button is pressed, power goes to pin 8 and becomes HIGH.
  {
    digitalWrite(led, HIGH);
    delay(500);
    digitalWrite(led, LOW);
    delay(500);
  }
  else // else if the button is not pressed, power does not go to pin 8 and becomes LOW.
  {
    digitalWrite(led, LOW);
  }
}




