const char START_CHAR = '%'; // Every new calculation needs to start with a '%'
const char END_CHAR = '#'; // Every new calculation needs to end with a '#'

bool reading = false; // Needed for the protocol.

int currentNumber = 0; // Indicating which number is read.

String number1_String = ""; // Fill this string with the first number.
String number2_String = ""; // Fill this string with the second number.
char requestedOperation = ' '; // Assign the character of the operation to do (+, -, *, /) to this variable.
float result = 0; // Initialize the result variable.

void setup()
{
  Serial.begin(9600); // Open the serial port.
}

void loop()
{
  if (Serial.available() > 0) //  Check if there are characters in the buffer.
  {
    char readChar = Serial.read(); // Read the character.

    /*
      END OF MESSAGE
    */
    if (reading == true && readChar == END_CHAR) // The full calculation is read.
    {
      reading = false; // No more reading for this calculation.

      // Call the function we made.
      calculate(); // Calculate the result bases on the variables number1_String, number2_String and requestedOperation.

      // Print the result.
      Serial.print(number1_String + requestedOperation + number2_String);
      Serial.print(" = ");
      Serial.println(result);
    }

    /*
      READING THE MESSAGE
    */
    if (reading == true)
    {
      // Check for the reqeusted operation and switch to the next number if the operator is read.
      // The numbers string are later converted to numbers so the calculation can be done.
      if (readChar == '+' || readChar == '-'
          || readChar == '*' || readChar == '/')
      {
        requestedOperation = readChar; // Store the operator.
        currentNumber++; // Increase by one to switch storing the next number.
      }
      else if (currentNumber == 0)
      {
        number1_String += readChar; // Append the read number to number1_String.
      }
      else if (currentNumber == 1)
      {
        number2_String += readChar; // Append the read number to number2_String.
      }
    }

    /*
      START OF MESSAGE
    */
    if (reading == false && readChar == START_CHAR)
    {
      // Reset the variables if a new calculation was received.
      currentNumber = 0;

      number1_String = "";
      number2_String = "";
      requestedOperation = ' ';
      result = 0;
      
      reading = true; // When everything is reset reading can begin.
    }
  }
}

void calculate()
{
  float number1 = number1_String.toFloat(); // Convert the string to float.
  float number2 = number2_String.toFloat(); // The float data type hold decimal numbers.

  switch (requestedOperation) // Check which operation was requested.
  {
    case '+':
      result = number1 + number2;
      break;

    case '-':
      result = number1 - number2;
      break;

    case '*':
      result = number1 * number2;
      break;

    case '/':
      if (number2 != 0) // Dividing by 0 isn't possible so check for this.
      {
        result = number1 / number2;
      }
      else
      {
        Serial.println("Can't devide by 0");
      }
      break;

    default:
      Serial.println("No valid operator was given");
      break;
  }
}


